import React, { Component } from 'react'
import { Header, Container, Input, Segment, Statistic, StatisticGroup } from 'semantic-ui-react'

import './App.css';
import '../semantic/dist/semantic.min.css';

const Rearth = 3959; // miles

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 0,
      distance: '',
      hiddenHeight: 'N/A',
      distanceToHorizon: 0,
      criticalDistance: this.calculateCriticalDistance(0),
    };
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(previousProps, previousState) {
    // console.log('state: ', this.state);
  }


  handleChange = (e, {name, value}) => {
    const newState = Object.assign(
      {},
      this.state,
      { [name]: value }
    );

    newState.distanceToHorizon = this.calculateDistanceToHorizon(newState.height);
    newState.criticalDistance = this.calculateCriticalDistance(newState.height);
    const hiddenHeight = this.calculateHiddenHeight(newState.height, newState.distance);
    if(hiddenHeight != null) {
      newState.hiddenHeight = hiddenHeight;
    }
    else {
      newState.hiddenHeight = 'N/A';
    }
    this.setState(newState);
  }

  calculateCriticalDistance = (height) => {
    const criticalDistance = this.calculateDistanceToHorizon(height) + Rearth*Math.PI/2;
    return criticalDistance;
  }

  calculateHiddenHeight = (height, distance) => {
    if (height >= 0 && distance) {
      const criticalDistance = this.calculateCriticalDistance(height);
      // If the target distance is at or beyond the critical distance, then the target
      // will always be hidden.
      if (distance >= criticalDistance) {
        return 'n/a';
      }
      const distanceToHorizon = this.calculateDistanceToHorizon(height);
      if (distanceToHorizon >= distance) {
        return 0;
      }

      // if we get here then it means that the distance to the object is beyond the horizon
      // so subtract the distance to horizon from the total distance
      const distanceInMiles = Rearth*(1/Math.cos((distance-distanceToHorizon)/Rearth) - 1);
      const distanceInFeet = distanceInMiles * 5280;
      return distanceInFeet;
    }
    else {
      return null;
    }
  };

  calculateDistanceToHorizon = (height) => {

    const heightInMiles = height/5280;
    return Rearth * Math.acos(Rearth/(Rearth + heightInMiles));
  }

  formatNumber = (number, decimals=4) => {
    if (isNaN(number)) {
      return number;
    }
    return Number(Math.round(number+'e'+decimals)+'e-'+decimals);
  }

  render() {
    return (
      <div>
        <Container className='overview'>
          <Header as='h1'>Earth Curvature Calculator</Header>
          <p>
            Calculates the distance to the horizon and the amount of hidden height of
            some target object given the observer's height and distance to the target.
          </p>
          <p>
            Distances are referenced along the curve of the earth. The implication of
            this is that there is a critical distance beyond which a target will always
            be hidden no matter how tall it is.
          </p>
          <p>
            Assumes that
          </p>
            <ol>
              <li>
                  Earth is a perfect sphere with a radius of {Rearth} miles.
              </li>
              <li>
                  No atmospheric effects (ex. refraction)
              </li>
            </ol>
          <a href="https://bitbucket.org/wyacteen/earth-curvature-calculator">Source</a> code hosted on Bitbucket.
          <br />
        </Container>
        <Container>
          <Segment.Group>
            <Segment>
              <div className='input'>
                <Input
                  label={{basic: true, content:'height (ft)'}}
                  labelPosition='right'
                  placeholder='observer eye height'
                  name='height'
                  value={this.state.height}
                  type='number'
                  min={0}
                  onChange={this.handleChange}
                  size='massive'
                />
              </div>
              <div>
                <Input
                  label={{basic: true, content:'target distance (mi)'}}
                  labelPosition='right'
                  placeholder='distance to target'
                  name='distance'
                  type='number'
                  value={this.state.distance}
                  min={0}
                  onChange={this.handleChange}
                  size='massive'
                />
              </div>
            </Segment>
            <Segment>
              <Header as='h2'>Results</Header>
              <StatisticGroup>
                <Statistic color='blue'>
                  <Statistic.Label>hidden height (feet)</Statistic.Label>
                  <Statistic.Value>{this.formatNumber(this.state.hiddenHeight)}</Statistic.Value>
                </Statistic>
              </StatisticGroup>
            </Segment>
            <Segment>
              <Header as='h2'>Other Details</Header>
              <StatisticGroup>
                <Statistic color='grey'>
                  <Statistic.Label>distance to horizon (miles)</Statistic.Label>
                  <Statistic.Value>{this.formatNumber(this.state.distanceToHorizon)}</Statistic.Value>
                </Statistic>
                <Statistic color='grey'>
                  <Statistic.Label>critical distance (miles)</Statistic.Label>
                  <Statistic.Value>{this.formatNumber(this.state.criticalDistance)}</Statistic.Value>
                </Statistic>
              </StatisticGroup>
            </Segment>
          </Segment.Group>
        </Container>
      </div>
    )
  }
}

  export default App;